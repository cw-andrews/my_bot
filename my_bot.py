import os

import maya
from ciscosparkbot import SparkBot


# Get Bot Identity from Environment Variables
bot_email = os.getenv("SPARK_BOT_EMAIL")
spark_token = os.getenv("SPARK_BOT_TOKEN")
bot_url = os.getenv("SPARK_BOT_URL")
bot_app_name = os.getenv("SPARK_BOT_APP_NAME")


# def chuck_joke(message):
#     # Use urllib to get a random joke
#     import urllib
#     import json
#
#     response = urllib.request.urlopen('http://api.icndb.com/jokes/random')
#     joke = json.loads(response.read())["value"]["joke"]
#
#     # Return the text of the joke
#     return joke


def get_time(message):
    return maya.time.asctime()


# Create New Bot Object
bot = SparkBot(bot_app_name,
               spark_bot_token=spark_token,
               spark_bot_url=bot_url,
               spark_bot_email=bot_email)

# Teach bot to tell Chuck Norris Jokes
# bot.add_command("/chuck", "Return a random Chuck Norris joke.", chuck_joke)
bot.add_command("/time", "Return the current datetime.", get_time)

# Start Your Bot
bot.run(host='0.0.0.0', port=5000)

