FROM python:3.6-jessie
MAINTAINER CW Andrews <cwandrews.oh@gmail.com>
EXPOSE 5000

# Create bot directory and add code
RUN mkdir /bot
WORKDIR /bot

# Install Bot Requirements
ADD ./requirements.txt /bot
RUN apt install gcc
RUN pip install -r /bot/requirements.txt

# Add Bot Code
ADD ./my_bot.py /bot

# Run Bot
CMD [ "python", "my_bot.py" ]
